1.  gPXE/PXE

2.  UEFI

3.  TFTP,DHCP(bootp)

\------------------------------

Installation from flash drive
=============================


1.  Download an image of OS

2.  Flash the image on a pen drive

3.  Configure BIOS to boot from flash or choose installation media from boot
    menu

4.  When you\`ll be promoted to partition disk choose manual partitioning

5.  Create partition in 'ext4' FS for '/' mount point

6.  Create partition for 'swap' in 'linux-swap'

